// accordion
$(document).ready(function() {
  $(".set > a").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this)
        .siblings(".content")
        .slideUp(200);
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
    } else {
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
      $(".set > a").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this)
        .siblings(".content")
        .slideDown(200);
    }
  });
});

// dark theme
function toggleDarkLight() {
  var body = document.getElementById("body");
  var currentClass = body.className;
  body.className = currentClass == "dark-mode" ? "light-mode" : "dark-mode";
}

// book search
function bookSearch(){
  var input = document.getElementById('search').value
  document.getElementById('results').innerHTML = ""
  console.log(input)

  if(input == null || input == ""){
    alert("Oops! you didn't search anything")
  } else{
    $.ajax({
      url: "https://www.googleapis.com/books/v1/volumes?q=" + input,
      dataType: "json",

      success: function(data){
        for(i = 0; i<data.items.length; i++){
          item = data.items[i]
          title = item.volumeInfo.title
          author = (item.volumeInfo.authors) ? item.volumeInfo.authors : "-"
          publisher = item.volumeInfo.publisher
          bookImg = (item.volumeInfo.imageLinks) ? item.volumeInfo.imageLinks.thumbnail : "https://via.placeholder.com/150" 
          bookLink = item.volumeInfo.previewLink

          results.innerHTML += "<tr><td><img src =" + bookImg + "></td><td>" + title + "</td><td>" + author + "</td><td>" + publisher + "</td><td><a href=" + bookLink + " class='btn btn-secondary'>Read More</a></td><tr>"
            
        }
      },

      type: 'GET'
    });
  }
}

document.getElementById('button-booksearch').addEventListener('click', bookSearch, false)

$(document).ready(function() {
  document.getElementById('results').innerHTML = ""

  $.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=harrypotter",
    dataType: "json",

    success: function(data){
      for(i = 0; i<data.items.length; i++){
        item = data.items[i]
        title = item.volumeInfo.title
        author = (item.volumeInfo.authors) ? item.volumeInfo.authors : "-"
        publisher = item.volumeInfo.publisher
        bookImg = (item.volumeInfo.imageLinks) ? item.volumeInfo.imageLinks.thumbnail : "https://via.placeholder.com/150" 
        bookLink = item.volumeInfo.previewLink

        results.innerHTML += "<tr><td><img src =" + bookImg + "></td><td>" + title + "</td><td>" + author + "</td><td>" + publisher + "</td><td><a href=" + bookLink + " class='btn btn-secondary'>Read More</a></td><tr>"
          
      }
    },

    type: 'GET'
  });
});