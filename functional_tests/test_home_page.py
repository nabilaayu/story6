from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from tugas.models import Status
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from django.test import Client
import time

class TestHomePage(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.client = Client()
    
    def tearDown(self):
        self.browser.close()

    def test_make_status(self):
        self.browser.get(
            '%s%s' % (self.live_server_url,  "/status/")
        )
        
        page = self.browser.find_element_by_class_name('padding-welcome')
        self.assertEquals(
            page.find_element_by_tag_name('h1').text,
            'Halo, apa kabar?'
        )

    def test_add_status_url(self):
        self.browser.get(
            '%s%s' % (self.live_server_url,  "/status/")
        )

        add_url = self.live_server_url + reverse('tugas:status')

        element = WebDriverWait(self.browser, 30).until(EC.visibility_of_element_located((By.XPATH, "//button[contains(.,'Add Status')]")))
        element.click()
        self.assertEquals(
            self.browser.current_url,
            add_url
        )
    
    def test_add_coba(self):
        self.browser.get(
            '%s%s' % (self.live_server_url,  "/status/")
        )

        add_url = self.live_server_url + reverse('tugas:status')
        response = self.client.post(add_url, {
            'text' : 'Coba Coba'
        })

        element = WebDriverWait(self.browser, 30).until(EC.visibility_of_element_located((By.XPATH, "//button[contains(.,'Add Status')]")))
        element.click()
        status = Status.objects.get(pk=1)
        self.assertEquals(
            str(status),
            'Coba Coba'
        )

    
