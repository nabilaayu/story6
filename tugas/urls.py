from django.urls import path
from . import views

app_name = 'tugas'

urlpatterns = [
    path('', views.home, name='index'),
    path('about/', views.about, name='about'),
    path('status/', views.status, name='status'),
    path('library/', views.library, name='library'),
]