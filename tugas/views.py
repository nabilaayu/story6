from django.shortcuts import render,get_object_or_404
from .models import Status
from .forms import StatusForm

# Create your views here.
def home(request):
    return render(request, 'pages/home.html', {})

def status(request):
    status = Status.objects.all()
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            form = StatusForm()
    else:
        form = StatusForm()
    return render(request, 'pages/status.html', {'status': status, 'form':form})

def about(request):
    return render(request, 'pages/about.html', {})

def library(request):
    return render(request, 'pages/library.html', {})