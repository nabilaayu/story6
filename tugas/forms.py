from django import forms
from .models import Status

##Create your forms here
class StatusForm(forms.ModelForm):

    class Meta:
        model = Status
        fields = ('text',)