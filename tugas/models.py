from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):
    text = models.TextField(u'Status:', max_length=300)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.text