from django.test import TestCase, Client
from django.urls import reverse
from tugas.models import Status

class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.index_url = reverse('tugas:index')
        self.status_url = reverse('tugas:status')
        self.about_url = reverse('tugas:about')
        self.library_url = reverse('tugas:library')
        self.login_url = reverse('login')

    def test_home(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/home.html')

    def test_status(self):
        response = self.client.get(self.status_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/status.html')

    def test_post(self):
        response = self.client.post(self.status_url, {
            'text' : 'bebas aja statusnya'
        })

        status = Status.objects.get(pk=1)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(str(status), 'bebas aja statusnya')

    def test_about(self):
        response = self.client.get(self.about_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/about.html')

    def test_library(self):
        response = self.client.get(self.library_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'pages/library.html')

    def test_login(self):
        response = self.client.get(self.login_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')
