from django.test import SimpleTestCase
from django.urls import reverse, resolve
from tugas.views import *
from django.contrib.auth.views import *

class TestUrls(SimpleTestCase):

    def test_home_urls_is_resolved(self):
        url = reverse('tugas:index')
        self.assertEquals(resolve(url).func, home)

    def test_status_urls_is_resolved(self):
        url = reverse('tugas:status')
        self.assertEquals(resolve(url).func, status)

    def test_about_urls_is_resolved(self):
        url = reverse('tugas:about')
        self.assertEquals(resolve(url).func, about)

    def test_library_urls_is_resolved(self):
        url = reverse('tugas:library')
        self.assertEquals(resolve(url).func, library)