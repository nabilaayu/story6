from django.test import SimpleTestCase
from tugas.forms import StatusForm

class TestForms(SimpleTestCase):

    def test_form_valid(self):
        form = StatusForm(data={
            'text' : 'bebas aja statusnya'
        })

        self.assertTrue(form.is_valid())

    def test_form_no_data(self):
        form = StatusForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)